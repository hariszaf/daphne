ODtoGDW=0.33
volExt = 0.03
volUn = 'L'
maxtime = 10.5
minstep = 0.0
nsteps = 10000
expcondLabels = {'batch_low_Glc': 'grown on 15 mM Glc',
                 'fedbatch_low_Ac': 'grown on 15 mM Glc, fed 4 mM Ac',
                 'fedbatch_high_Ac': 'grown on 15 mM Glc, fed 32 mM Ac'}

