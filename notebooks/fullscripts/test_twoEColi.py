# Change the absolute path of your daphne folder

DAPHNEPATH='/home/succurro/repositories/gitlab/daphne/'

ONETRANSDEF=False
ONETRANSDEF=True

# Load all the needed modules
import sys
sys.path.append(DAPHNEPATH+'code/python/')
import classModel as cmo
import classReaction as cre
import classMetabolite as cme
import classConsortium as cco
import classPlotter as plotter
import classConvertionFactors as ccf
import cobra
import pandas
from cobra.flux_analysis import parsimonious
import numpy as np
import matplotlib.pyplot as plt
import argparse
import plstyles
import random
import cPickle
import json
import copy
from matplotlib import gridspec
## SBML model to load
mpath = DAPHNEPATH+'ecoli/bigg/e_coli_core.xml'

## Biomass function name and reaction names in the SBML model
bmf = 'BIOMASS_Ecoli_core_w_GAM'
rxnnames = {'EX_glc_e':'EX_glc__D_e', 
            'EX_o2_e': 'EX_o2_e',
            'EX_ac_e':'EX_ac_e'}

## General labels
label='diauxic_shift'
ename = 'ecoli core'

# Initialize parameters environment and condition dependent to 0
expcond = 'NA'
biomass0 = 0.
glucose0 = 0.0 #mM
acetate0 = 0.0 #mM
ac_thr = 0.0
# Glucose fed-batch paramter
fb=0.
# Acetate feeding paramter
afb=0.
# Time at which glucose is exhausted and acetate feeding starts (Enjalbert et al. 2015 conditions)
t_glc = 0.0
# percentage of population in the glucose consuming state
pcECgl = 0.0

# Approximate convertion valute from OD to gDW measurement
ODtoGDW=0.33
# Biomass dilution rate in chemostat, 1/hr
ch = 0.
# Cell death rate
death_rate = -0.03
# Conditions from Enjalbert et al. 2015
volExt = 0.03
volUn = 'L'
expcondLabels = {'batch_low_Glc': 'grown on 15 mM Glc',
                 'fedbatch_low_Ac': 'grown on 15 mM Glc, fed 4 mM Ac',
                 'fedbatch_high_Ac': 'grown on 15 mM Glc, fed 32 mM Ac'}

# Efficiency for population transition, see later for explanation
transition_efficiency = 0.9


# Set flags for the 3 conditions of interest
# Set one condition flag to True
runbatchglc = False
runfedlowacetate = False
runfedhighacetate = True

if runbatchglc:
    print('Simulating Batch Growth on 15mM Glucose as in Enjalbert2015')
    expcond = 'batch_low_Glc'
    biomass0 = 0.0027*0.95
    glucose0 = 15.0 #mM
    acetate0 = 0.0 #mM
    fb = 0.
    afb = 0.
    pcECgl = 0.95
elif runfedlowacetate:
    print('Simulating Batch Growth on 15mM Glucose and constant 4mM Acetate as in Enjalbert2015')
    expcond = 'fedbatch_low_Ac'
    biomass0 = 0.0038*0.95
    glucose0 = 15.0
    acetate0 = 0.0
    afb = 1.0
    ac_thr = 4.0
    t_glc = 3.8
    pcECgl = 0.95
elif runfedhighacetate:
    print('Simulating Batch Growth on 15mM Glucose and constant 32mM Acetate as in Enjalbert2015')
    expcond = 'fedbatch_high_Ac'
    biomass0 = 0.006*0.75
    glucose0 = 15.0
    acetate0 = 32.0
    afb = 1.0
    ac_thr = 32.0
    t_glc = 3.3
    pcECgl = 0.75
else:
    print('Simulating custom conditions')

## Units:
## biomass0 is already gDW
## glucose0 and acetate0 are concentrations (mM)
## dMetabolites are initialized with quantities (mmol)
acetate0 = volExt*acetate0 # mM*L = mmol
glucose0 = volExt*glucose0 # mM*L = mmol

## Fixed FBA related parameters
## Uptake Vmax 10 mmol/g/hr (Gosset, 2005)
## Uptake Km 10 muM = 0.01 mM (Gosset, 2005)
vmaxexglc = 10. #mmol/g/hr
vmaxexace = 10. #mmol/g/hr
kmuptake = 0.01 #mM
## Parametrized with Varma 1994
vminoxygen = -11.5
ubexace = 3.0

biomass_ECgl = cme.Biomass([biomass0*pcECgl], {'growth': [(1, 'biomass_ECgl')], 'death': [(death_rate, 'biomass_ECgl')],
                                            'psi_transition': [(-1., 'biomass_ECgl')], 'phi_transition': [(transition_efficiency, 'biomass_ECac')]})
ex_glucose_ECgl = cme.DMetabolite('glc_D_e', [glucose0], False, {'glucose_exchange': [(1, 'biomass_ECgl')], 'glucose_fed': [(fb, None)] })
ex_acetate_ECgl = cme.DMetabolite('ac_e', [acetate0], False, {'acetate_exchange': [(1, 'biomass_ECgl')]})

oxygen_exchange_ECgl = cre.DReaction(rxnnames['EX_o2_e'], cre.FixedBound(vminoxygen, 0), False)
### Only Ac secretion
acetate_exchange_ECgl = cre.DReaction(rxnnames['EX_ac_e'], cre.FixedBound(0., ubexace), True)
glucose_exchange_ECgl = cre.DReaction(rxnnames['EX_glc_e'], cre.MichaelisMenten1(ex_glucose_ECgl, 1, vmaxexglc, kmuptake, 1, upperBound=15.), False)
growth_ECgl = cre.DReaction(bmf, cre.FixedBound(0., 1000.))


biomass_ECac = cme.Biomass([biomass0*(1-pcECgl)], {'growth': [(1, 'biomass_ECac')], 'death': [(death_rate, 'biomass_ECac')],
                                                'psi_transition': [(transition_efficiency, 'biomass_ECgl')], 'phi_transition': [(-1., 'biomass_ECac')]})
ex_glucose_ECac = cme.DMetabolite('glc_D_e', [glucose0], False, {'glucose_exchange': [(1, 'biomass_ECac')] })
ex_acetate_ECac = cme.DMetabolite('ac_e', [acetate0], False, {'acetate_exchange': [(1, 'biomass_ECac')], 'acetate_fed': [(afb, 'biomass_ECac')]})
    
oxygen_exchange_ECac = cre.DReaction(rxnnames['EX_o2_e'], cre.FixedBound(vminoxygen, 0), False)
acetate_exchange_ECac = cre.DReaction(rxnnames['EX_ac_e'], cre.MichaelisMenten1(ex_acetate_ECac, 1, vmaxexace, kmuptake, 1, upperBound=ubexace), False)
### EX_glc off
glucose_exchange_ECac = cre.DReaction(rxnnames['EX_glc_e'], cre.FixedBound(0.,0.), False)
growth_ECac = cre.DReaction(bmf, cre.FixedBound(0., 1000.))
acetate_fed_ECac = cre.DReaction(None, cre.SquareWave(9.1, 20, 1, t_glc), True, isODE=True)


# Set flags for the 3 types of transitions
# Set one transition flag to True
# Else, default is no transition
notransition = False
stochastictransition = False
responsivetransition = True

psi0 = 0.
psi_transition_rate = 0.
psi_transition_KM = 0.
phi0 = 0.
phi_transition_rate = 0.
phi_transition_KM = 0.

if stochastictransition:
    psi0=0.04
    phi0=0.04
elif responsivetransition:
    psi0=0.04
    phi0=0.04
    psi_transition_KM=30.0
    phi_transition_KM=5.0
    psi_transition_rate=0.2
    phi_transition_rate=0.2

## The Hill coefficient is heuristically set to 5
hctrans = 5

## phi = Glc -> Ac ##
## ECglc senses Ac and transitions
biomass_psi_transition_ECgl = cre.DReaction(None, cre.MichaelisMentenLinked(ex_acetate_ECgl, lowerBound=0.,
                                                                            maxVelocity=psi_transition_rate, hillCoeff=hctrans,
                                                                            mmConstant=psi_transition_KM, linkedReaction=None, onThr=0., offset=psi0), True, isODE=True)
biomass_psi_transition_ECac = cre.DReaction(None, cre.MichaelisMentenLinked(ex_acetate_ECgl, lowerBound=0.,
                                                                            maxVelocity=psi_transition_rate, hillCoeff=hctrans,
                                                                            mmConstant=psi_transition_KM, linkedReaction=None, onThr=0., offset=psi0), True, isODE=True)
## phi = Ac -> Glc ##
## ECac senses Glc and transitions
biomass_phi_transition_ECac = cre.DReaction(None, cre.MichaelisMentenLinked(ex_glucose_ECac, lowerBound=0.,
                                                                            maxVelocity=phi_transition_rate, hillCoeff=hctrans,
                                                                            mmConstant=phi_transition_KM, linkedReaction=None, onThr=0., offset=phi0), True, isODE=True)
biomass_phi_transition_ECgl = cre.DReaction(None, cre.MichaelisMentenLinked(ex_glucose_ECac, lowerBound=0.,
                                                                            maxVelocity=phi_transition_rate, hillCoeff=hctrans,
                                                                            mmConstant=phi_transition_KM, linkedReaction=None, onThr=0., offset=phi0), True, isODE=True)
exitname = '%s-%s-%s' % (ename, expcond, label) 

if ONETRANSDEF:
    ##### TEST
    biomass_psi_transition = cre.DReaction(None, cre.MichaelisMentenLinked(ex_acetate_ECgl, lowerBound=0.,
                                                                       maxVelocity=psi_transition_rate, hillCoeff=hctrans, mmConstant=psi_transition_KM,
                                                                       linkedReaction=None, onThr=0., offset=psi0), True, isODE=True)

    biomass_phi_transition = cre.DReaction(None, cre.MichaelisMentenLinked(ex_glucose_ECac, lowerBound=0.,
                                                                       maxVelocity=phi_transition_rate, hillCoeff=hctrans, mmConstant=phi_transition_KM,
                                                                       linkedReaction=None, onThr=0., offset=phi0), True, isODE=True)


    dyRxn_ECgl = {'glucose_exchange': glucose_exchange_ECgl, 
                  'oxygen_exchange': oxygen_exchange_ECgl,
                  'acetate_exchange': acetate_exchange_ECgl,
                  'psi_transition': biomass_psi_transition,
                  'phi_transition': biomass_phi_transition,
                  'growth': growth_ECgl}
    dyMet_ECgl = {'biomass_ECgl': biomass_ECgl,
                  'biomass_ECac': biomass_ECac,
                  'ex_glucose': ex_glucose_ECgl,
                  'ex_acetate': ex_acetate_ECgl}

    model_ECgl = cmo.DynamicModel(dyRxn_ECgl, dyMet_ECgl, mpath, volExt, volUn, 'optlang-glpk', exitname+'_ECgl', savePath='./')

    dyRxn_ECac = {'glucose_exchange': glucose_exchange_ECac, 
                  'oxygen_exchange': oxygen_exchange_ECac,
                  'acetate_fed': acetate_fed_ECac,
                  'acetate_exchange': acetate_exchange_ECac,
                  'psi_transition': biomass_psi_transition,
                  'phi_transition': biomass_phi_transition,
                  'growth': growth_ECac}
    dyMet_ECac = {'biomass_ECac': biomass_ECac,
                  'biomass_ECgl': biomass_ECgl,
                  'ex_glucose': ex_glucose_ECac,
                  'ex_acetate': ex_acetate_ECac}

    model_ECac = cmo.DynamicModel(dyRxn_ECac, dyMet_ECac, mpath, volExt, volUn, 'optlang-glpk', exitname+'_ECac', savePath='./')


    model_ECgl.loadSBMLModel()
    model_ECgl.resetObjective()
    model_ECgl.setObjective(growth_ECgl.modName, 1.)
    model_ECgl.constrainSources(element='C', count=0, uptakeOff=True)
    model_ECgl.initializeConcentrations()
    model_ECgl.setParsimoniousFBA(True)
    model_ECgl.setMinimalMaintenance(growth_ECgl.modName, -0.15)
    model_ECgl.setQuitOnFBAError(False)
    model_ECgl.setBoundsThreshold(0.8*vmaxexglc)
    model_ECgl.cbModel.reactions.get_by_id('ATPM').upper_bound = 8.39
    model_ECgl.cbModel.reactions.get_by_id('ATPM').lower_bound = 8.39

    model_ECac.loadSBMLModel()
    model_ECac.resetObjective()
    model_ECac.setObjective(growth_ECac.modName, 1.)
    model_ECac.constrainSources(element='C', count=0, uptakeOff=True)
    model_ECac.initializeConcentrations()
    model_ECac.setParsimoniousFBA(True)
    model_ECac.setMinimalMaintenance(growth_ECac.modName, -0.15)
    model_ECac.setQuitOnFBAError(False)
    model_ECac.setBoundsThreshold(0.8*vmaxexace)
    model_ECac.cbModel.reactions.get_by_id('ATPM').upper_bound = 8.39
    model_ECac.cbModel.reactions.get_by_id('ATPM').lower_bound = 8.39

    model_comm = cco.Consortium({'ECgl': model_ECgl, 'ECac': model_ECac},
                                {'biomass_ECgl': ['ECgl', 'ECac'], 
                                 'biomass_ECac': ['ECac', 'ECgl'],
                                 'ex_glucose': ['ECgl', 'ECac'],
                                 'ex_acetate': ['ECgl', 'ECac']
                                }, 'optlang-glpk', name=exitname+'_ECgl_ECac_v2_', savePath='./')

else:
    dyRxn_ECgl = {'glucose_exchange': glucose_exchange_ECgl, 
      'oxygen_exchange': oxygen_exchange_ECgl,
      'acetate_exchange': acetate_exchange_ECgl,
      'psi_transition': biomass_psi_transition_ECgl,
      'phi_transition': biomass_phi_transition_ECgl,
      'growth': growth_ECgl}
    dyMet_ECgl = {'biomass_ECgl': biomass_ECgl,
      'ex_glucose': ex_glucose_ECgl,
      'ex_acetate': ex_acetate_ECgl}

    model_ECgl = cmo.DynamicModel(dyRxn_ECgl, dyMet_ECgl, mpath, volExt, volUn, 'optlang-glpk', exitname+'_ECgl', savePath='./')

    dyRxn_ECac = {'glucose_exchange': glucose_exchange_ECac, 
      'oxygen_exchange': oxygen_exchange_ECac,
      'acetate_fed': acetate_fed_ECac,
      'acetate_exchange': acetate_exchange_ECac,
      'psi_transition': biomass_psi_transition_ECac,
      'phi_transition': biomass_phi_transition_ECac,
      'growth': growth_ECac}
    dyMet_ECac = {'biomass_ECac': biomass_ECac,
      'ex_glucose': ex_glucose_ECac,
      'ex_acetate': ex_acetate_ECac}

    model_ECac = cmo.DynamicModel(dyRxn_ECac, dyMet_ECac, mpath, volExt, volUn, 'optlang-glpk', exitname+'_ECac', savePath='./')
    
    model_ECgl.loadSBMLModel()
    model_ECgl.resetObjective()
    model_ECgl.setObjective(growth_ECgl.modName, 1.)
    model_ECgl.constrainSources(element='C', count=0, uptakeOff=True)
    model_ECgl.initializeConcentrations()
    model_ECgl.setParsimoniousFBA(True)
    model_ECgl.setMinimalMaintenance(growth_ECgl.modName, -0.15)
    model_ECgl.setQuitOnFBAError(False)
    model_ECgl.setBoundsThreshold(0.8*vmaxexglc)
    model_ECgl.cbModel.reactions.get_by_id('ATPM').upper_bound = 8.39
    model_ECgl.cbModel.reactions.get_by_id('ATPM').lower_bound = 8.39

    model_ECac.loadSBMLModel()
    model_ECac.resetObjective()
    model_ECac.setObjective(growth_ECac.modName, 1.)
    model_ECac.constrainSources(element='C', count=0, uptakeOff=True)
    model_ECac.initializeConcentrations()
    model_ECac.setParsimoniousFBA(True)
    model_ECac.setMinimalMaintenance(growth_ECac.modName, -0.15)
    model_ECac.setQuitOnFBAError(False)
    model_ECac.setBoundsThreshold(0.8*vmaxexace)
    model_ECac.cbModel.reactions.get_by_id('ATPM').upper_bound = 8.39
    model_ECac.cbModel.reactions.get_by_id('ATPM').lower_bound = 8.39

    model_comm = cco.Consortium({'ECgl': model_ECgl, 'ECac': model_ECac},
                                {'biomass_ECgl': ['ECgl'], 
                                 'biomass_ECac': ['ECac'],
                                 'ex_glucose': ['ECgl', 'ECac'],
                                 'ex_acetate': ['ECgl', 'ECac']
                                }, 'optlang-glpk', name=exitname+'_ECgl_ECac_', savePath='./')


maxtime = 10.5
minstep = 0.0
nsteps = 10000

model_comm.runConsortiumDynamicFBA(maxtime, 'vode', (0, float(minstep), 1., int(nsteps)), verbose=False)
